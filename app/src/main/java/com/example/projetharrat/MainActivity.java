package com.example.projetharrat;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    private boolean JOUER;
    
    private int cpt=0;
    private int niveau=0;
    private int potions, charme, salle_aleatoire_potion, salle_aleatoire_charme =0;
    
    
    private String PUISSANCE="";
    private String POINT_DE_VIE="";
    private String PUISSANCE_MONSTRE="150";
    private String PRENOM="";
    
    private TextView piece; /*Nombre de pièces*/
    private TextView puissance; /*Puissance du joueur*/
    private TextView pointsVie; /*point de vie du joueur*/
    private TextView affichage; /*Zone affichage de la batille ou de la guerre*/
     
    private HashMap<Integer, Integer> tableau_puissance_du_monstre = new HashMap<>();
    private HashMap<Integer, Integer> tableau_image_du_monstre = new HashMap<>();
    private HashMap<Integer, String> tableau_nom_du_monstre = new HashMap<>();
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
		
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        JOUER = true;
        niveau = 1;
        
        Button salle1 = (Button) findViewById(R.id.salle1);
        salle1.setOnClickListener((View.OnClickListener)this);

        Button salle2 = (Button) findViewById(R.id.salle2);
        salle2.setOnClickListener((View.OnClickListener) this);

        Button salle3 = (Button) findViewById(R.id.salle3);
        salle3.setOnClickListener((View.OnClickListener) this);

        Button salle4 = (Button) findViewById(R.id.salle4);
        salle4.setOnClickListener((View.OnClickListener) this);

        Button salle5 = (Button) findViewById(R.id.salle5);
        salle5.setOnClickListener((View.OnClickListener) this);

        Button salle6 = (Button) findViewById(R.id.salle6);
        salle6.setOnClickListener((View.OnClickListener) this);

        Button salle7 = (Button) findViewById(R.id.salle7);
        salle7.setOnClickListener((View.OnClickListener) this);

        Button salle8 = (Button) findViewById(R.id.salle8);
        salle8.setOnClickListener((View.OnClickListener) this);

        Button salle9 = (Button) findViewById(R.id.salle9);
        salle9.setOnClickListener((View.OnClickListener) this);

        Button salle10 = (Button) findViewById(R.id.salle10);
        salle10.setOnClickListener((View.OnClickListener) this);

        Button salle11 = (Button) findViewById(R.id.salle11);
        salle11.setOnClickListener((View.OnClickListener) this);

        Button salle12 = (Button) findViewById(R.id.salle12);
        salle12.setOnClickListener((View.OnClickListener) this);

        Button salle13 = (Button) findViewById(R.id.salle13);
        salle13.setOnClickListener((View.OnClickListener) this);

        Button salle14 = (Button) findViewById(R.id.salle14);
        salle14.setOnClickListener((View.OnClickListener) this);

        Button salle15 = (Button) findViewById(R.id.salle15);
        salle15.setOnClickListener((View.OnClickListener) this);

        Button salle16 = (Button) findViewById(R.id.salle16);
        salle16.setOnClickListener((View.OnClickListener) this);
        
        /*SALLE*/
        salle_grises(); /* Initialisation des salles de départ : Grise (état non visité)*/
        piece_a_seize(); /*Initialisation des pièces non visité à 16*/
        message_vide();
        
        /*JOUEUR*/
        puissance_a_100(); /*Puissance du joueur*/
		point_de_vie_a_10(); /*Points de vie du joueur*/
       
       
       /*MONSTRE*/
        nom_du_monstre(); /*Initalisation de tous les monstres*/
        puissance_a_zero(); /*Puissance du monstre*/
        salle_image(); /*initialisation de toutes les images des monstres*/
        
        /*POTION ET CHARME*/
        potion_et_charme(); /*Attribution des salles aléatoires et des valeurs pour la potion et le charme*/


    }
	/*HashMap de la puissance des monstres*/
    public void puissance_a_zero(){
        tableau_puissance_du_monstre.put(R.id.salle1,0);
        tableau_puissance_du_monstre.put(R.id.salle2,0);
        tableau_puissance_du_monstre.put(R.id.salle3,0);
        tableau_puissance_du_monstre.put(R.id.salle4,0);
        tableau_puissance_du_monstre.put(R.id.salle5,0);
        tableau_puissance_du_monstre.put(R.id.salle6,0);
        tableau_puissance_du_monstre.put(R.id.salle7,0);
        tableau_puissance_du_monstre.put(R.id.salle8,0);
        tableau_puissance_du_monstre.put(R.id.salle9,0);
        tableau_puissance_du_monstre.put(R.id.salle10,0);
        tableau_puissance_du_monstre.put(R.id.salle11,0);
        tableau_puissance_du_monstre.put(R.id.salle12,0);
        tableau_puissance_du_monstre.put(R.id.salle13,0);
        tableau_puissance_du_monstre.put(R.id.salle14,0);
        tableau_puissance_du_monstre.put(R.id.salle15,0);
        tableau_puissance_du_monstre.put(R.id.salle16,0);
    }

	/*Initialisation de toutes les images des monstres*/
    public void salle_image(){
        tableau_image_du_monstre.put(R.id.salle1,R.drawable.lawandowski);
        tableau_image_du_monstre.put(R.id.salle2,R.drawable.poty);
        tableau_image_du_monstre.put(R.id.salle3,R.drawable.sondi);
        tableau_image_du_monstre.put(R.id.salle4,R.drawable.bouneffa);
        tableau_image_du_monstre.put(R.id.salle5,R.drawable.verel);
        tableau_image_du_monstre.put(R.id.salle6,R.drawable.bourguin);
        tableau_image_du_monstre.put(R.id.salle7,R.drawable.robilliard);
        tableau_image_du_monstre.put(R.id.salle8,R.drawable.fonlupt);
        tableau_image_du_monstre.put(R.id.salle9,R.drawable.ramat);
        tableau_image_du_monstre.put(R.id.salle10,R.drawable.renaud);
        tableau_image_du_monstre.put(R.id.salle11,R.drawable.teytaud);
        tableau_image_du_monstre.put(R.id.salle12,R.drawable.dezecache);
        tableau_image_du_monstre.put(R.id.salle13,R.drawable.bourel);
        tableau_image_du_monstre.put(R.id.salle14,R.drawable.stubb);
        tableau_image_du_monstre.put(R.id.salle15,R.drawable.sadok);
        tableau_image_du_monstre.put(R.id.salle16,R.drawable.chotard);
    }

	/*Initalisation de tous les monstres*/
    public void nom_du_monstre(){
        tableau_nom_du_monstre.put(R.id.salle1,"BOSS : Lewandowski");
        tableau_nom_du_monstre.put(R.id.salle2,"BOSS : Poty");
        tableau_nom_du_monstre.put(R.id.salle3,"BOSS : Sondi");
        tableau_nom_du_monstre.put(R.id.salle4,"BOSS : Bouneffa");
        tableau_nom_du_monstre.put(R.id.salle5,"BOSS : Verel");
        tableau_nom_du_monstre.put(R.id.salle6,"BOSS : Bourguin");
        tableau_nom_du_monstre.put(R.id.salle7,"BOSS : Robillard");
        tableau_nom_du_monstre.put(R.id.salle8,"BOSS : Fonlupt");
        tableau_nom_du_monstre.put(R.id.salle9,"BOSS : Ramat");
        tableau_nom_du_monstre.put(R.id.salle10,"BOSS : Renaud");
        tableau_nom_du_monstre.put(R.id.salle11,"BOSS : Teytaud");
        tableau_nom_du_monstre.put(R.id.salle12,"BOSS : Dezecache");
        tableau_nom_du_monstre.put(R.id.salle13,"BOSS : Bourel");
        tableau_nom_du_monstre.put(R.id.salle14,"BOSS : Stubbe");
        tableau_nom_du_monstre.put(R.id.salle15,"BOSS : Sadok");
        tableau_nom_du_monstre.put(R.id.salle16,"BOSS : Chotard");
    }

	/*Attribution des salles aléatoires et des valeurs pour la potion et le charme*/
    public void potion_et_charme(){
		
        Object[] salle = tableau_puissance_du_monstre.keySet().toArray();
        Random random = new Random();
        
        /*Attribution de la salle alétaoire pour la potion*/
        salle_aleatoire_potion = (Integer)salle[new Random().nextInt(salle.length)];
        /*Valeur aléatoire de la potion de vie*/
        potions = 1+random.nextInt(3);
        
        /*Attribution de la salle alétaoire pour le charme*/
        salle_aleatoire_charme = (Integer)salle[new Random().nextInt(salle.length)];
        /*Valeur aléatoire du charme de puissance*/
        charme = random.nextInt((10 - 5) + 1) + 5;
    }

	/*Initialisation des pièces non visité à 16*/
    public void piece_a_seize(){
        piece = (TextView) findViewById(R.id.pièces);
        piece.setText("16");
    }

	/*Initialisation de la puissance du joueur à 100*/
    public void puissance_a_100 ()
    {
        puissance = (TextView) findViewById(R.id.puissance);
        puissance.setText("100");
    }

	/*Initialisation des points de vie à 10*/
    public void point_de_vie_a_10(){
        pointsVie= (TextView) findViewById(R.id.pointsVie);
        pointsVie.setText("10");
    }

	/*Message de résultat de la bataille (victoire, défaite ou fuite) 
	 * OU 
	 * de la guerre (Victoire ou défaite)
	 * initialisé à vide au début du jeu*/
    public void message_vide(){
        affichage= (TextView) findViewById(R.id.affichage);
        affichage.setText("");
    }


	/*Initialisation des salles non visité au début du jeu ou nouvelle partie*/
    public void salle_grises(){
        Button salle = (Button) findViewById(R.id.salle1);
        salle.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.gris));
        salle = (Button) findViewById(R.id.salle2);
        salle.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.gris));
        salle = (Button) findViewById(R.id.salle3);
        salle.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.gris));
        salle = (Button) findViewById(R.id.salle4);
        salle.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.gris));
        salle = (Button) findViewById(R.id.salle5);
        salle.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.gris));
        salle = (Button) findViewById(R.id.salle6);
        salle.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.gris));
        salle = (Button) findViewById(R.id.salle7);
        salle.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.gris));
        salle = (Button) findViewById(R.id.salle8);
        salle.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.gris));
        salle = (Button) findViewById(R.id.salle9);
        salle.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.gris));
        salle = (Button) findViewById(R.id.salle10);
        salle.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.gris));
        salle = (Button) findViewById(R.id.salle11);
        salle.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.gris));
        salle = (Button) findViewById(R.id.salle12);
        salle.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.gris));
        salle = (Button) findViewById(R.id.salle13);
        salle.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.gris));
        salle = (Button) findViewById(R.id.salle14);
        salle.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.gris));
        salle = (Button) findViewById(R.id.salle15);
        salle.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.gris));
        salle = (Button) findViewById(R.id.salle16);
        salle.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.gris));


    }

	/*Initialisation du niveau au début du jeu*/
    public  void niveau(){
        TextView niveau= (TextView) findViewById(R.id.bouton_niveau);
        niveau.setText("1");

    }


	/*Que faut il faire lorsqu'un joueur augmente de niveau ?
	 * Remettre les salles à l'état 'non visité'
	 * Remettre le nombre de pièces non visité à 16
	 * Permettre au joueur de joueur à nouveau (case cliquable)
	 * Le compteur des vicoires se remet à 0
	 */
    public void niveau_suivant(){
        salle_grises();
        piece_a_seize();
        JOUER=true;
        cpt=0;
    }

	/* Lorsque le joueur modifie ses paramètres*/
    public void a_jour(){
		
		/*Modification du PRENOM du joueur par la valeur récupéré dans les paramètres*/
        TextView prenom = (TextView) findViewById(R.id.prenom);
        prenom.setText(PRENOM);
        
		/*Modification de la PUISSANCE du joueur par la valeur récupéré dans les paramètres*/
		TextView puissance_du_joueur = (TextView) findViewById(R.id.puissance);
        puissance_du_joueur.setText(PUISSANCE);

		/*Modification des POINTS DE VIE du joueur par la valeur récupéré dans les paramètres*/
        TextView point_de_vie = (TextView) findViewById(R.id.pointsVie);
        point_de_vie.setText(POINT_DE_VIE);

    }

	/*Lancement d'une nouvelle partie via le menu
	 * Que faut il faire lorsqu'un joueur lance une nouvelle partie ?
	 * Remettre les salles à l'état 'non visité'
	 * Remettre le message de bataille ou guerre à vide
	 * Redonner une nouvelle puissance aléatoire à chaque monstres
	 * Remttre la puissance ud joueur à 100
	 * Remettre les points de vie du joueur à 10
	 * Remettre à jour l'affichage et remettre le joueur au niveau 1
	 */
	
	public void nouvelle_partie(){
		/*SALLE*/
		salle_grises();
		message_vide();
		piece_a_seize();
		JOUER=true;
		cpt=0;
		
		/*MONSTRE*/
        puissance_a_zero();
        
        /*JOUEUR*/
		puissance_a_100();
        point_de_vie_a_10();
        
        /*NIVEAU*/
        niveau();
        niveau=1;

	}

	/*Création du menu*/
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main,menu);
        return true;
    }

	/*Création des catégories du menu*/
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        switch(id){
			/*Nouvelle partie*/
            case R.id.nouveau:
                nouvelle_partie();
                return true;
            /*Mes paramètres*/
            case R.id.parametres:
                boite_de_dialogue_parametres();
                return true;
            case R.id.sauvegarde:
                 boite_de_dialogue_sauvegarder();
            default:
                return false;
        }
    }

    public void boite_de_dialogue_sauvegarder(){

    }


	/*Boite de dialogue pour 'Mes paramètres*/
    public void boite_de_dialogue_parametres(){
		
		/*Création de l'AlertDialog*/
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

		/*Création d'un LinearLayout sur lequel les TextView et les EditText vont être 'accroché'*/
        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);

        final TextView prenom_text = new TextView(this);
        prenom_text.setHint("Prénom du joueur");
        layout.addView(prenom_text);

		
        final EditText prenom = new EditText(this);
        prenom.setHint("Zohra");
        /*Clavier Alphabetique*/
        prenom.setInputType(1);
        layout.addView(prenom);

        final TextView puissance_joueur_text = new TextView(this);
        puissance_joueur_text.setHint("Puissance du joueur");
        layout.addView(puissance_joueur_text);

        final EditText puissance_joueur = new EditText(this);
        puissance_joueur.setHint("100");
        /*Clavier numérique*/
        puissance_joueur.setInputType(2);
        layout.addView(puissance_joueur);

        final TextView point_de_vie_text = new TextView(this);
        point_de_vie_text.setHint("Point de vie du joueur");
        layout.addView(point_de_vie_text);

        final EditText point_de_vie = new EditText(this);
        point_de_vie.setHint("150");
        /*Clavier numérique*/
        point_de_vie.setInputType(2);
        layout.addView(point_de_vie);

        final TextView puissance_monstre_text = new TextView(this);
        puissance_monstre_text.setHint("Puissance du monstre");
        layout.addView(puissance_monstre_text);

        final EditText puissance_monstre = new EditText(this);
        puissance_monstre.setHint("10");
        puissance_monstre.setInputType(2);
        layout.addView(puissance_monstre);

		builder
		/*Titre de la boite de dialogue*/
		.setTitle("Paramètres")
		/*Icon présent à côté du titre*/
		.setIcon(R.drawable.parametre)
		/*On accroche le layout*/
		.setView(layout)
		/*Si on valide le formulaire, que se passe t-il ?*/
		.setPositiveButton("Valider",new DialogInterface.OnClickListener(){
			
			public void onClick(DialogInterface dialog, int which){
				
				/*Récupération du prénom, des puissances et des points de vie présent dans le layout*/
				String prenom_du_joueur = String.valueOf(prenom.getText());
				int puissance_du_joueur, point_de_vie_du_joueur, puissance_du_monstre ;

				/*Vérification : Il ne faut pas que les champs soient vide*/
				if(!puissance_joueur.getText().toString().equals(""))
					puissance_du_joueur = Integer.parseInt(puissance_joueur.getText().toString());
				else
					puissance_du_joueur=0;

				if(!point_de_vie.getText().toString().equals(""))
					point_de_vie_du_joueur = Integer.parseInt(point_de_vie.getText().toString());
				else
					point_de_vie_du_joueur = 0;

				if(!puissance_monstre.getText().toString().equals(""))
					puissance_du_monstre = Integer.parseInt(puissance_monstre.getText().toString());
				else 
					puissance_du_monstre=0;
				
				if(prenom_du_joueur != null && !prenom_du_joueur.equals("")
					&& puissance_du_joueur>0
					&& point_de_vie_du_joueur >0
					&& puissance_du_monstre >0
				   )
				{
					/*Stocker les valeurs*/
					PUISSANCE = Integer.toString(puissance_du_joueur);
					PUISSANCE_MONSTRE = Integer.toString(puissance_du_monstre);
					POINT_DE_VIE = Integer.toString(point_de_vie_du_joueur);
					PRENOM = prenom_du_joueur;
					/*Met à jour ces modification dans la première activité*/
					a_jour();
				}
               }
			})
			/*Si le jour annule le formulaire, rien ne se passe*/
			.setNegativeButton("Annuler",null)
			/*Affichage*/
			.show();
    }

	/*Lorsqu'un joueur clique sur un élément de la première activité*/
    public void onClick(View v){

		/*Si le joueur clique sur un bouton pour entrer dans une salle
		 * et qu'il a l'autorisation d'y accéder (case cliquable)*/
        if((v.getId() == R.id.salle1 || v.getId() == R.id.salle2 || v.getId() == R.id.salle3 ||
			v.getId() == R.id.salle4 || v.getId() == R.id.salle5 || v.getId() == R.id.salle6 ||
			v.getId() == R.id.salle7 || v.getId() == R.id.salle8 || v.getId() == R.id.salle9 ||
			v.getId() == R.id.salle10 || v.getId() == R.id.salle11 || v.getId() == R.id.salle12 ||
			v.getId() == R.id.salle13 || v.getId() == R.id.salle14 || v.getId() == R.id.salle15 ||
			v.getId() == R.id.salle16 )&& JOUER == true){

			/*Récupère la salle en cours de visite*/
            Button salle = (Button) findViewById(v.getId());
            
            /*Si cette salle est verte, le joueur est déjà entré dedans et à battu le monstre
             * il ne peut donc plus y accéder*/
            if(salle.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.vert2).getConstantState())){
                Toast.makeText(this,"Vous avez déjà réussi cet examen.",Toast.LENGTH_SHORT).show();
            }
            /*S'il n'a jamais visité la salle*/
            else{
				/*Création de l'intention qui contiendra tous les éléments à envoyer à la seconde activité*/
                Intent intent = new Intent(this,SecondActivity.class);

				pointsVie = (TextView) findViewById(R.id.pointsVie);
                puissance = (TextView) findViewById(R.id.puissance);
                
				/*Envoie toutes ces données à la seconde activité*/

                /*JOUEUR*/
                intent.putExtra("nom_du_joueur",PRENOM);
                intent.putExtra("point de vie",pointsVie.getText().toString());
                intent.putExtra("puissance joueur",puissance.getText().toString());

                /*MONSTRE*/
                intent.putExtra("nom_du_monstre",tableau_nom_du_monstre.get(v.getId()));
                intent.putExtra("puissance_du_monstre",tableau_puissance_du_monstre.get(v.getId()));
                intent.putExtra("image_du_monstre",tableau_image_du_monstre.get(v.getId()));
                intent.putExtra("puissance_du_monstre_MAXIMUM",PUISSANCE_MONSTRE);

                /*SALLE*/
                intent.putExtra("salle",v.getId());

                /*POTION*/
                intent.putExtra("salle_potions_aleatoire",salle_aleatoire_potion);
                intent.putExtra("potions",potions);

                /*CHARME*/
                intent.putExtra("salle_charme_aleatoire",salle_aleatoire_charme);
                intent.putExtra("charme",charme);

                /*NIVEAU*/
                intent.putExtra("niveau",niveau);
                
                // va sur la second activité
                startActivityForResult(intent,1);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        TextView pdv = (TextView) findViewById(R.id.pointsVie);
        TextView puiss = (TextView) findViewById(R.id.puissance);

        int point_de_vie = Integer.parseInt(pdv.getText().toString());
        int puissance_joueur = Integer.parseInt(puiss.getText().toString());

        TextView pi = (TextView) findViewById(R.id.pièces);
        int pieces = Integer.parseInt(piece.getText().toString());

        if(data.getStringExtra(SecondActivity.RESULTAT).equals("fuite"))
        {
            //Récupération de la valeur
            point_de_vie = point_de_vie-1;
            String vie = Integer.toString(point_de_vie);
            pdv.setText(vie);
            if(point_de_vie <=0){
                TextView message = (TextView) findViewById(R.id.affichage);
                message.setText("Vous devez aller au rattrapage ...");
                JOUER = false;

            }
            else{
                TextView message = (TextView) findViewById(R.id.affichage);
                message.setText("Mince ! J'essaierai au rattrapage ...");
            }
            Button salle = (Button) findViewById(data.getIntExtra("salle_changer", 0));
            salle.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.orange2));
            int adver = data.getIntExtra("puissance_monstre",0);
            tableau_puissance_du_monstre.put(data.getIntExtra("salle_changer", 0), adver);
        }

        if(data.getStringExtra(SecondActivity.RESULTAT).equals("defaite")){
            //Récupération de la valeur
            point_de_vie = point_de_vie-3;
            String vie = Integer.toString(point_de_vie);
            pdv.setText(vie);

            if(point_de_vie <=0){
                TextView message = (TextView) findViewById(R.id.affichage);
                message.setText("Vous devez aller au rattrapage.");
                JOUER = false;
            }
            else{
                TextView message = (TextView) findViewById(R.id.affichage);
                message.setText("Je me suis fait prendre ... !");
            }
            Button salle = (Button) findViewById(data.getIntExtra("salle_changer", 0));
            salle.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.orange2));
            int adver = data.getIntExtra("puissance_monstre",0);
            tableau_puissance_du_monstre.put(data.getIntExtra("salle_changer", 0), adver);

        }

        if(data.getStringExtra(SecondActivity.RESULTAT).equals("victoire")){
            cpt++;
            pieces -= 1;
            String nombre_pieces = Integer.toString(pieces);
            pi.setText(nombre_pieces);

            int salle_avant = data.getIntExtra("salle_changer", 0);

            if(salle_avant == salle_aleatoire_potion){
                point_de_vie = point_de_vie + potions;
                String point = Integer.toString(point_de_vie);
                pointsVie.setText(point);
            }

            if(salle_avant == salle_aleatoire_charme){
                puissance_joueur = puissance_joueur + charme;
                String puissance = Integer.toString(puissance_joueur);
                puiss.setText(puissance);
            }

            puissance_joueur = puissance_joueur + 10 ;
            String puissance = Integer.toString(puissance_joueur);
            puiss.setText(puissance);

            Button salle = (Button) findViewById(salle_avant);
            salle.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.vert2));

            if(cpt<16){
                TextView message = (TextView) findViewById(R.id.affichage);
                message.setText("Yes ! 20/20 ");
            }
            else{
                TextView message = (TextView) findViewById(R.id.affichage);
                message.setText("Bravo ! Vous avez votre diplôme.");
                JOUER = false;
                niveau=niveau+1;
                Button butt = (Button) findViewById(R.id.bouton_niveau);
                String niveau_du_joueur = Integer.toString(niveau);
                butt.setText(niveau_du_joueur);
                niveau_suivant();
            }


        }



    }



}
