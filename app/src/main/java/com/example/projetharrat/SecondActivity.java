package com.example.projetharrat;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import org.w3c.dom.Text;

import java.util.Random;

public class SecondActivity extends AppCompatActivity implements View.OnClickListener {

    static final String RESULTAT = "résultat";
    private int valeur_niveau = 0;
    int nb=0;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_layout);

        Button attaque = (Button) findViewById(R.id.attaque);
        attaque.setOnClickListener((View.OnClickListener)this);


        Button fuite = (Button) findViewById(R.id.fuite);
        fuite.setOnClickListener((View.OnClickListener) this);

        TextView vie = (TextView) findViewById(R.id.pointsVieJ);
        vie.setText(getIntent().getStringExtra("point de vie"));

        TextView puissance_J = (TextView) findViewById(R.id.puissanceJ);
         puissance_J.setText(getIntent().getStringExtra("puissance joueur"));


        int puissanceM = getIntent().getIntExtra("puissance_du_monstre",0);
        if(puissanceM == 0 ){
            //Puissance Adverssaire
            TextView puissanceAleatoireAdverssaire= (TextView) findViewById(R.id.puissanceA);
            Random random = new Random();

            int valeur = getIntent().getIntExtra("niveau",0);
            String valeur_MAX = getIntent().getStringExtra("puissance_du_monstre_MAXIMUM");
            int maximum = Integer.parseInt(valeur_MAX);
            nb = random.nextInt(maximum + ((valeur -1 ) *160 ))+1;
            valeur_niveau = nb + valeur;
            String test = Integer.toString(valeur_niveau);
            puissanceAleatoireAdverssaire.setText(test);
        }
        else{
            TextView puissanceAdverssaire = (TextView) findViewById(R.id.puissanceA);
            puissanceAdverssaire.setText(Integer.toString(puissanceM));
        }
        ImageView imageView = (ImageView) findViewById(R.id.image_monstre);
        int image = getIntent().getIntExtra("image_du_monstre",0);
        imageView.setImageResource(image);

        TextView nom_du_monstre = (TextView) findViewById(R.id.nom_du_monstre);
        String nom = getIntent().getStringExtra("nom_du_monstre");
        nom_du_monstre.setText(nom);

        TextView nom_du_joueur = (TextView) findViewById(R.id.nom_du_joueur);
        String nomJoueur = getIntent().getStringExtra("nom_du_joueur");
		nom_du_joueur.setText(nomJoueur);

        int salle = getIntent().getIntExtra("salle",0);
        int salle_aleatoire_potion = getIntent().getIntExtra("salle_potions_aleatoire",0);
        int salle_aleatoire_charme = getIntent().getIntExtra("salle_charme_aleatoire",0);
        if(salle == salle_aleatoire_potion ){
            ImageView image_potions_ou_charme = (ImageView) findViewById(R.id.image_potion);
            image_potions_ou_charme.setImageResource(R.drawable.potions);
            int pot = getIntent().getIntExtra("potions",0);
            TextView text = (TextView) findViewById(R.id.pointsVieJplus);
            String potion = Integer.toString(pot);
            text.setText("+"+potion);
        }
        if(salle == salle_aleatoire_charme ){
            ImageView image_potions_ou_charme = (ImageView) findViewById(R.id.image_charme);
            image_potions_ou_charme.setImageResource(R.drawable.charme);
            int charm = getIntent().getIntExtra("charme",0);
            TextView text = (TextView) findViewById(R.id.puissanceJplus);
            String charme = Integer.toString(charm);
            text.setText("+"+charme);
        }

    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        Intent intent = new Intent();
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            intent.putExtra(RESULTAT, "fuite");
            int intent2 = getIntent().getIntExtra("salle",0);
            intent.putExtra("salle_changer",intent2);
            setResult(RESULT_OK, intent);
            finish();
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent();
        TextView puissanceJ = (TextView) findViewById(R.id.puissanceJ);
        int puissance_J = Integer.parseInt(puissanceJ.getText().toString());
        TextView puissanceA = (TextView) findViewById(R.id.puissanceA);
        int puissance_A = Integer.parseInt(puissanceA.getText().toString());

        if (v.getId() == R.id.fuite ) {
            //résultat de la page : fuite puis active le onactivityresult
            intent.putExtra(RESULTAT, "fuite");
            int intent2 = getIntent().getIntExtra("salle",0);
            intent.putExtra("salle_changer",intent2);
            intent.putExtra("puissance_monstre",puissance_A);
            setResult(RESULT_OK, intent);
            finish();
        }



        double calcul = puissance_J * Math.random() - puissance_A * Math.random();

        if (v.getId() == R.id.attaque && calcul >= 0){


            intent.putExtra(RESULTAT, "victoire");
            int intent2 = getIntent().getIntExtra("salle",0);
            intent.putExtra("salle_changer",intent2);
            setResult(RESULT_OK, intent);
            finish();
        }
        if (v.getId() == R.id.attaque && calcul < 0){
           intent.putExtra(RESULTAT,"defaite");
            int intent2 = getIntent().getIntExtra("salle",0);
            intent.putExtra("salle_changer",intent2);
            intent.putExtra("puissance_monstre",puissance_A);



            setResult(RESULT_OK,intent);
            finish();
        }

    }


}
